package com.synoptica.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public abstract class Page {

  protected WebDriver driver;

  protected WebDriverWait wait;



  public Page(WebDriver driver) {
    this.driver = driver;
    this.wait = new WebDriverWait(driver, 5);
  }

  public WebDriver getDriver() {
    return driver;
  }

  public String getTitle() {
    return driver.getTitle();
  }

  public String getUrl() {
    return driver.getCurrentUrl();
  }

//  public Boolean testAllLinks() {
//    Boolean allLinksWork = true;
//    List<String> links = getAndFilterLinks();
//    System.out.println("Testing of all links is commencing");
//      for (String link : links) {
//        try {
//          driver.get(link);
//          wait.until(ExpectedConditions.urlContains(link));
//        } catch (Exception e) {
//          System.out.println(e.getMessage());
//        }
//        try {
//          JsonError json = new JsonError(driver.getPageSource());
//          System.out.println("Link has failed: " + driver.getCurrentUrl());
//          System.out.println("-----Error details------");
//          System.out.println(json.getErrorCode() + " ; " + json.getStatus() + " ; " + json.getMessage());
//          System.out.println("---------------------");
//          allLinksWork = false;
//        } catch (JSONException e) {
//          System.out.println("No JSON means no error, all is good.");
//        }
//
//      }
//
//    return allLinksWork;
//  }

  public List<String> getAndFilterLinks() {
    List<String> links = new ArrayList<>();
    List<WebElement> elements = driver.findElements(By.cssSelector("a"));
    System.out.println("--------------------All Links found on page------------------");
    for (int i = 0; i < elements.size(); i++) {
        String tel = null;
        String mail = null;
        String lastChar = null;
        String hash = "#";
        String blank = "_blank";
        WebElement element = elements.get(i);
        System.out.println(element);
        if (elements.get(i).getAttribute("href") != null) {
          lastChar = element.getAttribute("href").substring(element.getAttribute("href").length() - 1);
          tel = element.getAttribute("href").substring(0, 3);
          mail = element.getAttribute("href").substring(0, 4);
        }
        System.out.println(element.getAttribute("href"));
      try {
        if (lastChar.equals(hash)) {
          System.out.println("This link cannot be tested: " + element);
        } else if (blank.equals(null)) {
          System.out.println("This link cannot be tested: " + element);
        } else if (tel.equals(new String("tel"))) {
          System.out.println("This link cannot be tested: " + element);
        } else if (mail.equals(new String("mail"))) {
          System.out.println("This link cannot be tested: " + element);
        } else {
          links.add(element.getAttribute("href"));
        }
      } catch (NullPointerException e) {
        System.out.println("Href attribute is null, not a real link.");
      }
    }

    System.out.println("------------------------------------");
    System.out.println("---------------- Final Links Here --------------------");
    if(links.size() == 0) {
      System.out.println("List is empty; No links.");
    } else {
      System.out.println("Size of list is: " + links.size());
      for (String link : links) {
        System.out.println("A link to be tested: " + link);
      }
    }
    System.out.println("------------------------------------");
    return links;
  }

}
