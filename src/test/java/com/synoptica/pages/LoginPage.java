package com.synoptica.pages;

import com.synoptica.Props;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.synoptica.Props;

public class LoginPage extends Page {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoginPage.class);

  @FindBy(how = How.NAME, using = "username")
  private WebElement usernameBox;

  @FindBy(how = How.NAME, using = "password")
  private WebElement passwordBox;

  @FindBy(how = How.ID, using = "login_button")
  private WebElement submitBtn;

  public LoginPage(WebDriver driver) {
    super(driver);
  }

  public DashboardPage loginValidUser(String username, String password) {
    doLogin(username, password);

    wait.until(ExpectedConditions.urlContains(Props.get("url.dashboard")));

    return PageFactory.initElements(driver, DashboardPage.class);
  }

//  public AdminDashboardPage loginAsAdmin() {
//    doLogin(Props.get("admin.username"), Props.get("admin.password"));
//    wait.until(ExpectedConditions.urlContains("/dashboard"));
//
//    return PageFactory.initElements(driver, AdminDashboardPage.class);
//  }


  private void doLogin(String username, String password) {
    System.out.println("Login credentials: " + username + " / " + password);

    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    usernameBox.sendKeys(username);
    passwordBox.sendKeys(password);
    submitBtn.click();
  }

  public LoginPage loginBadUser() {
    doLogin("foobaz92649k", "bazfoosas30494@");

    return this;
  }

}
