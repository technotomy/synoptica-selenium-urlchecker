package com.synoptica.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DashboardPage extends Page{

  @FindBy(how = How.XPATH, using = "//a[@href='/supplier/projects']")
  private WebElement opportunityButton;

  public DashboardPage(WebDriver webDriver) {
    super(webDriver);
  }

  public void goToOpportunities() {
    System.out.println("Going to Opportunity main page!");
    opportunityButton.click();
  }

}
