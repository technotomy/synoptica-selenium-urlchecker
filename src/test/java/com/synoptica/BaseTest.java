package com.synoptica;

import org.junit.*;
import org.junit.rules.Timeout;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseTest {
  protected final Logger logger = LoggerFactory.getLogger(BaseTest.class);
  protected static WebDriver driver;
  protected static String BASE_URL = Props.get("url.base");
  static int count;


  @BeforeClass
  public static void init() {
    count = 0;
    String path = System.getProperty("user.dir");
    System.out.println("OS: " + System.getProperty("os.name"));
    if (System.getProperty("os.name").equals("Linux")) {
      System.out.println("Driver at path: " + path + "/chromedriver2_30Linux");
      System.setProperty("webdriver.chrome.driver", path + "/chromedriver2_30Linux");
    } else if (System.getProperty("os.name").equals("Mac OS X")) {
      System.out.println("Driver at path: " + path + "/chromedriver2_30Mac");
      System.setProperty("webdriver.chrome.driver", path + "/chromedriver2_30Mac");
    }
  }

  @Before
  public void setup() {
    try {
      count++;
      ChromeOptions options = new ChromeOptions();
      options.addArguments("--headless");
      options.addArguments("--disable-gpu");
      driver = new ChromeDriver(options);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @AfterClass
  public static void teardown() {
    try {
      if (driver != null) {
        driver.quit();
      }
      System.out.println("Tests: " + count);
    }catch (Exception e) {
      e.printStackTrace();
    }
  }

  protected void goTo(String url) {
    logger.debug("goTo(), url = {}", BASE_URL + url);
    driver.get(BASE_URL + url);
  }

  @Rule
  public Timeout globalTimeout = Timeout.seconds(600);

}
