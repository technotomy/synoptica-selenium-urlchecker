package com.synoptica.tests;

import com.synoptica.BaseTest;
import com.synoptica.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class BaseDomainUrlChecking extends BaseTest {

    Set<String> checkSites = new HashSet<>();

    @Test
    public void checkBaseDomain() {
        driver.get("https://www.synoptica.com");
        Set<String> allLinks = crawlSite();
        int totalBrokenLinks = testLinks(allLinks);
        System.out.println("Number of checked URLs: " + checkSites.size());
        Assert.assertEquals("Some links were tested and responded with 4xx or 5xx status codes.", 0, totalBrokenLinks);
    }

    private Set<String> crawlSite() {
        Set<String> toReturn = new HashSet<>();
        Set<String> links = collectAndFilterLinks();
        toReturn.addAll(links);
        for (String link : links) {
            if (link.contains("logout")) continue;
            driver.get(link);
            Set<String> subLinks = collectAndFilterLinks();
            toReturn.addAll(subLinks);
        }

        Iterator<String> iterator = toReturn.iterator();
        while(iterator.hasNext()) {
            String setElement = iterator.next();
            if(setElement.contains("logout")) {
                iterator.remove();
            }
        }
        return toReturn;
    }


    public Set<String> collectAndFilterLinks() {
        List<WebElement> hyperlinkElements = driver.findElements(By.tagName("a"));
        Set<String> links = new HashSet<>();
        for (WebElement element : hyperlinkElements) {
            links.add(element.getAttribute("href"));
        }
        for (Iterator<String> i = links.iterator(); i.hasNext();) {
            String link = i.next();
            if (link != null) {
                if (link.contains("tel:") || link.contains("mailto:") || link.equals("")) {
                    i.remove();
                    continue;
                }
                if (link.contains("twitter") || link.contains("facebook") || link.contains("google") || link.contains("linkedin")) {
                    i.remove();
                    continue;
                }
                if (!link.contains("synoptica")) {
                    i.remove();
                    continue;
                }
            } else {
                i.remove();
            }
        }
        return links;
    }


    private int testLinks(Set<String> links) {
        int brokenLinkCount = 0;
        try {
            for (String link : links) {
                if (!Utils.doesSetContainString(checkSites, link)) {
                    System.out.println("Testing: " + link);
                    checkSites.add(link);
                    URL url = new URL(link);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.connect();
                    String response = connection.getResponseMessage();
                    String responseCode = String.valueOf(connection.getResponseCode());
                    if (responseCode.matches("4[0-9]{2}") || responseCode.matches("5[0-9]{2}")) {
                        System.out.println("This link is broken: " + link + " / " + response + " / " + responseCode);
                        brokenLinkCount++;
                    } else {
                        System.out.println("Response: " + response + " / " + responseCode);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return brokenLinkCount;
    }
}
