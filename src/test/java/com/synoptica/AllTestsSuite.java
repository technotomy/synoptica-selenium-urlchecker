package com.synoptica;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import com.synoptica.tests.DomainSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DomainSuite.class
         })
public class AllTestsSuite   {

}
