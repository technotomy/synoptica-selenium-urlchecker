package com.synoptica;

/**
 * Created by synoptica on 29/11/2016.
 */
public final class StaticConstants {

    static String clientEmail;
    static String supplierEmail;
    static String clientUsername;
    static String supplierUsername;
    static String clientCompanyName;
    static String supplierCompanyName;
    static String clientCompanyUrl;
    static String supplierCompanyUrl;
    static String urlPreviousIntent;

    public StaticConstants() {
        clientEmail = Utils.generateRandomEmail();
        supplierEmail = Utils.generateRandomEmail();
        clientUsername = Utils.generateRandomString();
        supplierUsername = Utils.generateRandomString();
        clientCompanyName = Utils.generateRandomString();
        supplierCompanyName = Utils.generateRandomString();
        clientCompanyUrl = Utils.generateRandomUrl();
        supplierCompanyUrl = Utils.generateRandomUrl();
        urlPreviousIntent = "";
    }

    public static void setPreviousUrlIntent(String newUrlIntent) {
        urlPreviousIntent = newUrlIntent;
    }

    public static String getClientEmail() {
        return clientEmail;
    }

    public static String getSupplierEmail() {
        return supplierEmail;
    }

    public static String getClientUsername() {
        return clientUsername;
    }

    public static String getSupplierUsername() {
        return supplierUsername;
    }

    public static String getClientCompanyName() {
        return clientCompanyName;
    }

    public static String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    public static String getClientCompanyUrl() {
        return clientCompanyUrl;
    }

    public static String getSupplierCompanyUrl() {
        return supplierCompanyUrl;
    }

    public static String getUrlPreviousIntent() {
        return urlPreviousIntent;
    }
}
