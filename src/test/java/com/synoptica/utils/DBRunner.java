//package synoptica.utils;
//
//import com.synoptica.Props;
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.FileNotFoundException;
//import java.io.InputStream;
//import java.io.Serializable;
//import java.sql.*;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Scanner;
//
//import static java.util.Objects.isNull;
//
//public class DBRunner implements Serializable {
//  private final HikariDataSource ds;
//
//  private static final Logger LOGGER = LoggerFactory.getLogger(DBRunner.class);
//
//  public DBRunner() {
//    HikariConfig config = new HikariConfig();
//    config.setJdbcUrl(getDbUrl());
//    config.setUsername(getDbUsername());
//    config.setPassword(getDbPass());
//    config.setDriverClassName("org.postgresql.Driver");
//    ds = new HikariDataSource(config);
//  }
//
//  public void shutDownPool() {
//    if (!isNull(ds)) {
//      ds.close();
//    }
//  }
//
//  public void runScript(String scriptName) throws SQLException, FileNotFoundException {
//    try (Connection connection = ds.getConnection()) {
//      LOGGER.debug("COOOOOOOOONNNNNNNNECTION!!!!");
//      InputStream is = DBRunner.class.getResourceAsStream(scriptName);
//      importSQL(connection, is);
//    }
//  }
//
//  public void executeSql(String Sql) throws SQLException {
//    Statement st = null;
//    System.out.println("SQL COMMAND: " + Sql);
//    try (Connection connection = ds.getConnection()) {
//      LOGGER.debug("COOOOOOOOONNNNNNNNECTION!!!!");
//      st = connection.createStatement();
//      int i = st.executeUpdate(Sql);
//      try {
//        Thread.sleep(2000);
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//
//      System.out.println("Number of updated records: " + i);
//      if (i==0) {
//          int j = st.executeUpdate(Sql);
//          System.out.println("Query resent " + j );
//      }
//
//    } finally {
//      if (st != null) st.closeOnCompletion();
//    }
//  }
//
//
//  public Map querySql(String Sql) throws SQLException {
//    Map<String, String> toReturn = new HashMap<>();
//
//    Connection c = null;
//    Statement stmt = null;
//    try {
//      Class.forName("org.postgresql.Driver");
//      c = DriverManager.getConnection(Props.get("db.url"), Props.get("db.username"), Props.get("db.pass"));
//      c.setAutoCommit(false);
//      System.out.println("Opened database successfully");
//
//      stmt = c.createStatement();
//      ResultSet rs = stmt.executeQuery(Sql);
//      ResultSetMetaData metaData = rs.getMetaData();
//      int columnsNumber = metaData.getColumnCount();
//      while ( rs.next() ) {
//        for (int i = 1; i <= columnsNumber; i++) {
//            String columnValue = rs.getString(i);
//            toReturn.put(metaData.getColumnName(i), columnValue);
//          }
//      }
//      rs.close();
//      stmt.close();
//      c.close();
//    } catch ( Exception e ) {
//      System.err.println( e.getClass().getName()+": "+ e.getMessage() );
//    }
//    System.out.println("Operation done successfully");
//
//    return toReturn;
//  }
//
//  public static void importSQL(Connection conn, InputStream in) throws SQLException {
//    Scanner s = new Scanner(in);
//    s.useDelimiter("(;(\r)?\n)|(--\n)");
//    Statement st = null;
//    try {
//      st = conn.createStatement();
//      while (s.hasNext()) {
//        String line = s.next();
//        if (line.startsWith("/*!") && line.endsWith("*/")) {
//          int i = line.indexOf(' ');
//          line = line.substring(i + 1, line.length() - " */".length());
//        }
//
//        if (line.trim().length() > 0) {
//          st.execute(line);
//        }
//      }
//    } finally {
//      if (st != null) st.close();
//    }
//  }
//
//  private String getDbUrl() {
//    return Props.get("db.url");
//  }
//  private String getDbUsername() {
//    return Props.get("db.username");
//  }
//  private String getDbPass() {
//    return Props.get("db.pass");
//  }
//}
