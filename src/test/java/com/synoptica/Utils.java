package com.synoptica;

import java.util.Random;
import java.util.Set;

public class Utils {
    public static String generateRandomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }

    public static String generateRandomEmail() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString() + "@tester.co.uk";
        return output;
    }

    public static String generateRandomUrl() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = "www." + sb.toString() + ".com";
        return output;
    }

    public static Boolean doesSetContainString(Set<String> set, String string) {
        Boolean toReturn = false;
        for(String str : set) {
            if (str.equals(string)){
                toReturn = true;
                break;
            }
        }
        return toReturn;
    }

}
