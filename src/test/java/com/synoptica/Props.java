package com.synoptica;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.IOException;
import java.util.Properties;

public class Props {

  private static String DEAFULT_PROP_FILE = "/local.properties";

  private Props() {
  }

  public static String get(String name) {
    Properties props = new Properties();
    try {
      props.load(Props.class.getResourceAsStream(getPropFilename()));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return props.getProperty(name);
  }

  public static void set(String key, String value) throws ConfigurationException {
    System.out.println("Update: " + System.getProperty("user.dir") + "/src/test/resources" + getPropFilename());
    PropertiesConfiguration config = new PropertiesConfiguration(System.getProperty("user.dir") + "/src/test/resources" + getPropFilename());
    config.setProperty(key, value);
    config.save();
    System.out.println("Config Property Key/Value pair Updated: " + key + "/" + value);
  }

  private static String getPropFilename() {
    String profile = System.getProperty("syn.maven.profile");
    String environment = System.getProperty("env");

    if (profile != null) {
      return "/" + profile + ".properties";
    } else if(environment != null){
      return "/" + environment + ".properties";
    }

    return DEAFULT_PROP_FILE;
  }

}
